import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

import '../source.dart';

abstract class BaseViewModel extends ChangeNotifier {
  final loadingSubject = BehaviorSubject<bool>();
  final errorSubject = BehaviorSubject<String>();

  late BuildContext _context;

  BuildContext get context => _context;

  setContext(BuildContext value) => _context = value;

  void setLoading(bool loading) {
    if (loading != isLoading) loadingSubject.add(loading);
  }

  bool get isLoading => loadingSubject.value;

  void setError(String message) => errorSubject.add(message);

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  @override
  void dispose() async {
    await loadingSubject.drain();
    loadingSubject.close();
    await errorSubject.drain();
    errorSubject.close();
    super.dispose();
  }

  void unFocus() {
    FocusScope.of(context).unfocus();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  Future<bool> showError({String? msg}) async {
    return await showDialog(
      context: context,
      builder: (c) => WidgetDialogError(
        title: msg ?? 'Video chưa sẵn sàng',
        onAction: () => Navigator.pop(context),
      ),
    );
  }
}
