import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../source.dart';

List<CalendarEvent> calendarEventFromJson(String str) =>
    List<CalendarEvent>.from(
      json.decode(str).map(
            (x) => CalendarEvent.fromJson(x),
          ),
    );

String calendarEventToJson(List<CalendarEvent> data) => json.encode(
      List<dynamic>.from(
        data.map(
          (x) => x.toJson(),
        ),
      ),
    );

class CalendarEvent {
  final String title;
  final String content;
  late String dateTime;
  final String hourTime;
  final int colors;

  CalendarEvent({
    required this.title,
    required this.content,
    required this.dateTime,
    required this.hourTime,
    required this.colors,
  });

  List<Color> _colors = [
    AppColors.blue,
    AppColors.lightOrange,
    AppColors.pink,
    AppColors.primaryLight,
  ];

  getColors() => _colors[colors];

  factory CalendarEvent.fromJson(Map<String, dynamic> json) {
    return CalendarEvent(
      title: json['title'],
      content: json['content'],
      dateTime: json['dateTime'],
      colors: json['colors'],
      hourTime: json['hourTime'],
    );
  }

  static List<CalendarEvent> listFromJson(dynamic json) => json != null
      ? List<CalendarEvent>.from(
          json.map((x) => CalendarEvent.fromJson(x)),
        )
      : [];

  Map<String, dynamic> toJson() => {
        'title': title,
        'content': content,
        'dateTime': dateTime,
        'hourTime': hourTime,
        'colors': colors,
      };
}
