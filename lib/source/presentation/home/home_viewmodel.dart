import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:toast/toast.dart';

import '../../source.dart';

class HomeViewModel extends BaseViewModel {
  CalendarController calendarController = CalendarController();
  List<Widget> children = [];
  Map<DateTime, List<dynamic>> events = {};
  List<dynamic> selectedEvents = [];
  List<CalendarEvent> eventsInYear = [
    CalendarEvent(
      title: "Giải phóng Miền Nam",
      content: "Giải phóng Miền Nam",
      dateTime: "04-30",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Ngày của mẹ",
      content: "Ngày của mẹ",
      dateTime: "05-13",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Ngày của cha",
      content: "Ngày của cha",
      dateTime: "06-17",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Ngày Quốc tế Phụ nữ",
      content: "Ngày Quốc tế Phụ nữ",
      dateTime: "03-08",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Ngày Quốc tế Hạnh phúc",
      content: "Ngày Quốc tế Hạnh phúc",
      dateTime: "03-20",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Tết Nguyên Tiêu",
      content: "Tết Nguyên Tiêu",
      dateTime: "01-15",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Tết Hàn Thực",
      content: "Tết Hàn Thực",
      dateTime: "03-03",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Tết Đoan Ngọ",
      content: "Tết Đoan Ngọ",
      dateTime: "05-05",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Giỗ tổ Hùng Vương",
      content: "Giỗ tổ Hùng Vương",
      dateTime: "04-02",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Lễ Phật Đản",
      content: "Lễ Phật Đản",
      dateTime: "04-15",
      hourTime: "00:00",
      colors: 0,
    ),
    CalendarEvent(
      title: "Ngày quốc tế lao động",
      content: "Ngày quốc tế lao động",
      dateTime: "05-01",
      hourTime: "00:00",
      colors: 1,
    ),
    CalendarEvent(
      title: "Lễ Quốc Khánh",
      content: "Lễ Quốc Khánh",
      dateTime: "09-02",
      hourTime: "00:00",
      colors: 1,
    ),
    CalendarEvent(
      title: "Tiễn Táo Quân về trời",
      content: "Tiễn Táo Quân về trời",
      dateTime: "12-23",
      hourTime: "00:00",
      colors: 1,
    ),
    CalendarEvent(
      title: "Ngày Halloween",
      content: "Ngày Halloween",
      dateTime: "10-31",
      hourTime: "00:00",
      colors: 1,
    ),
  ];

  DateTime now = DateTime.now();
  DateTime selectedDay = DateTime.now();

  init(Map<DateTime, List<dynamic>> events) async {
    selectedDay = DateTime(now.year, now.month, now.day, 12, 0, 0, 0, 0);
    this.events = events;
  }

  showDialogAddEvent() {
    showDialog(
      context: context,
      builder: (_) => WidgetDialogAddEvent(
        onTap: (calendarEvent) => addNewEvent(calendarEvent),
        selectDay: selectedDay,
      ),
    );
  }

  showDialogInformation(CalendarEvent calendarEvent) {
    showDialog(
      context: context,
      builder: (_) => WidgetDialogInformation(
        calendarEvent: calendarEvent,
      ),
    );
  }

  setSelectEvents(DateTime dt, List<dynamic> events) {
    selectedEvents = events;
    selectedDay = dt;
    notifyListeners();
  }

  addNewEvent(CalendarEvent calendarEvent) async {
    if (calendarController.selectedDay != null) {
      if (calendarEvent.title.isEmpty || calendarEvent.content.isEmpty) {
        Toast.show("You need to enter Title or Content", context);
      } else {
        var dt = calendarController.selectedDay;
        if (events[dt] != null) {
          events[dt]!.add(calendarEvent);
        } else {
          selectedEvents.add(calendarEvent);
          events[dt] = selectedEvents;
        }

        await AppShared.setEvent(events);
        Navigator.pop(context);
      }
    } else {
      Toast.show("You need to choose a day", context);
    }
  }
}
