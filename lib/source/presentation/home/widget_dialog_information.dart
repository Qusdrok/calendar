import 'package:flutter/material.dart';

import '../../source.dart';

class WidgetDialogInformation extends StatefulWidget {
  final CalendarEvent calendarEvent;

  const WidgetDialogInformation({
    Key? key,
    required this.calendarEvent,
  }) : super(key: key);

  @override
  _WidgetDialogInformationState createState() => _WidgetDialogInformationState();
}

class _WidgetDialogInformationState extends State<WidgetDialogInformation> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 8,
      backgroundColor: AppColors.primaryVeryLight,
      shape: RoundedRectangleBorder(
        side: BorderSide.none,
        borderRadius: BorderRadius.circular(15),
      ),
      insetPadding: EdgeInsets.symmetric(horizontal: 20),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Note Information",
              style: AppStyles.DEFAULT_LARGE,
            ),
            _buildRowInfo(
              title: "Title:",
              content: widget.calendarEvent.title,
            ),
            _buildRowInfo(
              title: "Content:",
              content: widget.calendarEvent.content,
            ),
            _buildRowInfo(
              title: "Time:",
              content: widget.calendarEvent.dateTime,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRowInfo({
    required String title,
    required String content,
  }) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.primary,
            ),
          ),
          Expanded(
            child: Text(
              content,
              style: AppStyles.DEFAULT_LARGE.copyWith(
                color: AppColors.primary,
              ),
              textAlign: TextAlign.end,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
