import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import '../../source.dart';

class HomeScreen extends StatefulWidget {
  final Map<DateTime, List<dynamic>> events;

  const HomeScreen({required this.events});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      viewModel: new HomeViewModel(),
      onViewModelReady: (viewModel) =>
          _viewModel = (viewModel as HomeViewModel)..init(widget.events),
      builder: (context, viewModel, child) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            backgroundColor: AppColors.primary,
            body: SafeArea(
              child: Stack(
                children: [
                  Positioned(
                    top: -40,
                    right: -40,
                    child: CircleAvatar(
                      radius: 52,
                      backgroundColor: AppColors.orange,
                    ),
                  ),
                  Column(
                    children: [
                      _buildAppBar(),
                      Expanded(child: _buildBody()),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppBar() {
    return Container(
      height: AppValues.HEIGHT_APP_BAR + 5,
      width: AppSize.screenWidth,
      padding: EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: 10),
              child: Text(
                "CALENDAR",
                style: AppStyles.DEFAULT_REGULAR_BOLD.copyWith(
                  color: AppColors.primaryVeryLight,
                ),
              ),
            ),
          ),
          Flexible(
            child: GestureDetector(
              onTap: () => _viewModel.showDialogAddEvent(),
              child: Icon(
                Icons.add,
                size: 30,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 12),
            child: _buildCalendarCarousel(),
          ),
          Container(
            color: AppColors.primaryDark,
            padding: EdgeInsets.symmetric(
              vertical: 30,
              horizontal: 15,
            ),
            child: StreamBuilder(
              stream: AppShared.getEventStream(_viewModel.selectedDay),
              builder: (context, snapshot) {
                bool enabled =
                    snapshot.data == null && snapshot.connectionState == ConnectionState.done;
                List<CalendarEvent> data = [];
                var selectedDay = _viewModel.selectedDay.toIso8601String();

                if (!enabled && snapshot.data != null && snapshot.data != "") {
                  data = calendarEventFromJson(snapshot.data.toString());
                }

                _viewModel.eventsInYear.forEach((element) {
                  if (selectedDay.contains(element.dateTime)) {
                    var dt = new DateFormat("MM-dd").parse(element.dateTime);
                    dt = DateTime(_viewModel.selectedDay.year, dt.month, dt.day);

                    var calendarEvent = new CalendarEvent(
                      title: element.title,
                      content: element.content,
                      dateTime: DateTimeHelper.ConvertDateTimeWithTextMonth(dt),
                      hourTime: element.hourTime,
                      colors: element.colors,
                    );

                    data.add(calendarEvent);
                  }
                });

                if (data.isNotEmpty) {
                  data.sort((a, b) => a.hourTime.compareTo(b.hourTime));
                }

                return enabled
                    ? WidgetShimmer(
                        child: Wrap(
                          runSpacing: 15,
                          children: List.generate(
                            10,
                            (index) => _buildEventItem(
                              calendarEvent: new CalendarEvent(
                                colors: 0,
                                title: "Meeting",
                                content: "Meeting",
                                dateTime: _viewModel.now.toString(),
                                hourTime: "15:00",
                              ),
                            ),
                          ),
                        ),
                      )
                    : data.length > 0
                        ? Wrap(
                            runSpacing: 15,
                            children: List.generate(
                              data.length,
                              (index) => _buildEventItem(
                                calendarEvent: data[index],
                              ),
                            ),
                          )
                        : Center(
                            child: Text(
                              "Không có ghi chú",
                              style: AppStyles.DEFAULT_LARGE.copyWith(
                                color: AppColors.white,
                              ),
                            ),
                          );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCalendarCarousel() {
    return TableCalendar(
      events: _viewModel.events,
      availableGestures: AvailableGestures.horizontalSwipe,
      calendarController: _viewModel.calendarController,
      initialSelectedDay: _viewModel.now,
      endDay: DateTime(9999),
      calendarStyle: CalendarStyle(
        canEventMarkersOverflow: true,
      ),
      builders: CalendarBuilders(
        outsideWeekendDayBuilder: (context, dt, _) => _buildCalendarText(
          text: "${dt.day}",
          color: AppColors.primaryVeryVeryLight,
        ),
        outsideDayBuilder: (context, dt, _) => _buildCalendarText(
          text: "${dt.day}",
          color: AppColors.primaryVeryVeryLight,
        ),
        dayBuilder: (context, dt, _) => _buildCalendarText(
          text: "${dt.day}",
          color: AppColors.white,
        ),
        weekendDayBuilder: (context, dt, events) => _buildCalendarText(
          text: "${dt.day}",
          color: AppColors.white,
        ),
        dowWeekendBuilder: (context, weekDay) => _buildCalendarText(
          text: "S",
          color: AppColors.white,
          height: 65,
          child: Container(
            height: 1,
            width: 50,
            color: AppColors.primaryVeryVeryLight,
            margin: EdgeInsets.only(
              left: weekDay.startsWith("Sat") ? 0 : 10,
              right: weekDay.startsWith("Sat") ? 10 : 0,
            ),
          ),
        ),
        markersBuilder: (context, dt, events, _) {
          List<Widget> marker = [];

          if (events.isNotEmpty) {
            var calendarEvent = null;
            if (events[0] is Map<String, dynamic>) {
              calendarEvent = CalendarEvent.fromJson(events[0]);
            } else {
              calendarEvent = events[0] as CalendarEvent;
            }

            marker.add(
              Container(
                decoration: BoxDecoration(
                  color: calendarEvent.getColors(),
                  borderRadius: BorderRadius.circular(60),
                ),
                alignment: Alignment.center,
                child: Text(
                  "${dt.day}",
                  style: AppStyles.DEFAULT_REGULAR.copyWith(
                    color: AppColors.white,
                  ),
                ),
              ),
            );
          }

          return marker;
        },
        dowWeekdayBuilder: (context, weekDay) => _buildCalendarText(
          text: "${weekDay[0]}",
          color: AppColors.white,
          height: 65,
          child: Container(
            height: 1,
            width: 50,
            color: AppColors.primaryVeryVeryLight,
          ),
        ),
        selectedDayBuilder: (context, dt, events) {
          return Container(
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(60),
            ),
            alignment: Alignment.center,
            child: Text(
              "${dt.day}",
              style: AppStyles.DEFAULT_REGULAR.copyWith(
                color: AppColors.white,
              ),
            ),
          );
        },
      ),
      onDaySelected: (dt, events, holidays) => _viewModel.setSelectEvents(dt, events),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        titleTextBuilder: (dt, _) => DateTimeHelper.ConvertMonthToEnglish(dt),
        titleTextStyle: AppStyles.DEFAULT_XLARGE_BOLD.copyWith(
          color: AppColors.white,
          fontSize: 25,
        ),
        leftChevronIcon: WidgetImageIcon(
          imageURL: AppImages.icChevronLeft,
          height: 20,
          width: 10,
        ),
        leftChevronPadding: EdgeInsets.only(left: 10),
        rightChevronIcon: WidgetImageIcon(
          imageURL: AppImages.icChevronRight,
          height: 20,
          width: 10,
        ),
        rightChevronPadding: EdgeInsets.only(right: 10),
        formatButtonVisible: false,
      ),
    );
  }

  Widget _buildCalendarText({
    required String text,
    Color? color,
    double height = 0,
    Widget? child,
    CrossAxisAlignment? alignmentChild,
  }) {
    return Container(
      alignment: Alignment.center,
      height: height,
      width: 50,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: alignmentChild ?? CrossAxisAlignment.center,
        children: [
          Text(
            text,
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: color ?? AppColors.primaryVeryLight,
            ),
          ),
          SizedBox(height: child != null ? 15 : 0),
          if (child != null) child,
        ],
      ),
    );
  }

  Widget _buildEventItem({required CalendarEvent calendarEvent}) {
    var color = calendarEvent.getColors();
    return GestureDetector(
      onTap: () => _viewModel.showDialogInformation(calendarEvent),
      child: Container(
        height: 75,
        width: AppSize.screenWidth,
        color: AppColors.primary,
        padding: EdgeInsets.all(15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundColor: color,
                    radius: 5,
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          calendarEvent.title,
                          style: AppStyles.DEFAULT_REGULAR.copyWith(
                            color: AppColors.white,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          calendarEvent.dateTime,
                          style: AppStyles.DEFAULT_REGULAR.copyWith(
                            color: AppColors.primaryVeryVeryLight,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 25,
              width: 55,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(60),
              ),
              child: Text(
                calendarEvent.hourTime,
                style: AppStyles.DEFAULT_REGULAR.copyWith(
                  color: AppColors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
