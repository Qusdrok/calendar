import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar/source/helper/app_helper.dart';
import 'package:flutter_calendar/source/source.dart';
import 'package:flutter_calendar/source/widget/widget_text_form_field.dart';

class WidgetDialogAddEvent extends StatefulWidget {
  final Function(CalendarEvent calendarEvent) onTap;
  final DateTime selectDay;

  WidgetDialogAddEvent({
    Key? key,
    required this.selectDay,
    required this.onTap,
  }) : super(key: key);

  @override
  _WidgetDialogAddEventState createState() => _WidgetDialogAddEventState();
}

class _WidgetDialogAddEventState extends State<WidgetDialogAddEvent>
    with SingleTickerProviderStateMixin {
  late TextEditingController titleController = TextEditingController();
  late TextEditingController contentController = TextEditingController();
  late List<Color> colors = [];
  late DateTime hourTime = DateTime.now();
  late AnimationController controller;
  late Animation<double> scaleAnimation;

  late int _selectedColor = 0;

  @override
  void initState() {
    super.initState();
    colors = [
      AppColors.blue,
      AppColors.lightOrange,
      AppColors.pink,
      AppColors.primaryLight,
    ];

    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );

    scaleAnimation = CurvedAnimation(
      parent: controller,
      curve: Curves.elasticInOut,
    );

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: ScaleTransition(
        scale: scaleAnimation,
        child: Dialog(
          elevation: 8,
          backgroundColor: AppColors.primaryVeryLight,
          shape: RoundedRectangleBorder(
            side: BorderSide.none,
            borderRadius: BorderRadius.circular(15),
          ),
          insetPadding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            padding: const EdgeInsets.all(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Add New Event",
                  style: AppStyles.DEFAULT_LARGE_BOLD,
                ),
                SizedBox(height: 15),
                WidgetTextFormField(
                  hintText: "Title",
                  controller: titleController,
                  circular: 10,
                ),
                SizedBox(height: 10),
                WidgetTextFormField(
                  hintText: "Content",
                  controller: contentController,
                  circular: 10,
                ),
                SizedBox(height: 15),
                Text(
                  "Select Color Event",
                  style: AppStyles.DEFAULT_LARGE_BOLD,
                ),
                SizedBox(height: 10),
                Wrap(
                  spacing: 10,
                  children: List.generate(
                    colors.length,
                    (index) => GestureDetector(
                      onTap: () => setState(() => _selectedColor = index),
                      child: CircleAvatar(
                        backgroundColor: AppColors.white,
                        radius: 25,
                        child: Stack(
                          children: [
                            CircleAvatar(
                              backgroundColor: colors[index],
                              radius: 24,
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.center,
                                child: Icon(
                                  const IconData(
                                    0xe156,
                                    fontFamily: 'MaterialIcons',
                                  ),
                                  size: 20,
                                  color: _selectedColor == index
                                      ? AppColors.white
                                      : Colors.transparent,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "Select Hour Time",
                  style: AppStyles.DEFAULT_LARGE_BOLD,
                ),
                SizedBox(height: 15),
                Container(
                  height: 100,
                  child: CupertinoDatePicker(
                    onDateTimeChanged: (dt) => hourTime = dt,
                    use24hFormat: true,
                    mode: CupertinoDatePickerMode.time,
                  ),
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    WidgetButtonGradientAnimation(
                      width: 85,
                      height: 35,
                      action: () =>
                          widget.onTap(
                            CalendarEvent(
                              title: titleController.text,
                              content: contentController.text,
                              dateTime:
                                  DateTimeHelper.ConvertDateTimeWithTextMonth(widget.selectDay),
                              hourTime:
                                  "${AppHelper.minimum(hourTime.hour)}:${AppHelper.minimum(hourTime.minute)}",
                              colors: _selectedColor,
                            ),
                          ) ??
                          {},
                      colorStart: AppColors.blue,
                      colorEnd: AppColors.primaryVeryVeryLight,
                      title: 'Add',
                      textStyle: AppStyles.DEFAULT_REGULAR.copyWith(
                        color: AppColors.white,
                      ),
                    ),
                    SizedBox(width: 30),
                    WidgetButtonGradientAnimation(
                      width: 85,
                      height: 35,
                      action: () => Navigator.pop(context),
                      colorStart: AppColors.primaryVeryVeryLight,
                      colorEnd: AppColors.blue,
                      title: 'Cancel',
                      textStyle: AppStyles.DEFAULT_REGULAR.copyWith(
                        color: AppColors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
