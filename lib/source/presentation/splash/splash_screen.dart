import 'package:flutter/material.dart';
import 'package:flutter_calendar/source/source.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late SplashViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget(
      viewModel: SplashViewModel(),
      onViewModelReady: (viewModel) => _viewModel = (viewModel as SplashViewModel)..init(),
      builder: (context, viewModel, child) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            body: SafeArea(
              child: Center(
                child: _buildBody(),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AppImages.logo,
            height: AppSize.screenHeight / 3,
            width: AppSize.screenWidth,
          ),
          SizedBox(height: 35),
          GestureDetector(
            onTap: () => _viewModel.openDynamicLink(0),
            child: Image.asset(
              AppImages.btnHome,
              height: 100,
              width: AppSize.screenWidth - 100,
            ),
          ),
          GestureDetector(
            onTap: () => _viewModel.openDynamicLink(1),
            child: Image.asset(
              AppImages.btnSupport,
              height: 100,
              width: AppSize.screenWidth - 100,
            ),
          ),
        ],
      ),
    );
  }
}
