import 'dart:async';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../source.dart';

class SplashViewModel extends BaseViewModel {
  late RemoteConfig remoteConfig;
  late Timer timer;
  String home = "";
  bool isChanging = false;

  Map<DateTime, List<dynamic>> events = {};
  List<String> links = [];
  List<String> dynamicLinks = [
    "https://fluttercalendar.page.link/home2",
    "https://fluttercalendar.page.link/home",
  ];

  init() async {
    setLoading(true);
    for (var link in dynamicLinks) {
      final PendingDynamicLinkData? data =
          await FirebaseDynamicLinks.instance.getDynamicLink(Uri.parse(link));
      var url = data?.link;
      if (url != null) links.add(url.toString());
    }

    await Future.delayed(Duration(milliseconds: 300));
    setLoading(false);

    timer = Timer.periodic(Duration(seconds: 5), (t) => openNewSplash());
  }

  openNewSplash() async {
    try {
      setLoading(true);
      isChanging = true;

      remoteConfig = await RemoteConfig.instance;
      var defaults = <String, dynamic>{
        'home': '',
      };

      await remoteConfig.setDefaults(defaults);
      remoteConfig.setConfigSettings(
        RemoteConfigSettings(
          minimumFetchIntervalMillis: 21600000,
          fetchTimeoutMillis: 30000,
        ),
      );

      await remoteConfig.fetch(expiration: Duration(hours: 0));
      await remoteConfig.activateFetched();

      home = remoteConfig.getString("home");
      await Future.delayed(Duration(milliseconds: 100));

      if (home == "false") {
        print("UI Home");
        timer.cancel();
        Navigator.pushReplacementNamed(
          context,
          Routers.splash2,
          arguments: true,
        );
      } else {
        print("UI Splash");
      }
    } on FetchThrottledException {
      await showError(msg: "Lỗi kết nối mạng");
    } catch (e) {
      await showError(msg: "Lỗi khác");
    }

    isChanging = false;
    setLoading(false);
  }

  openDynamicLink(int index) async {
    if (index > links.length - 1 || isChanging) return;
    var url = links[index];

    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: true,
        forceWebView: true,
        enableJavaScript: true,
      );
    } else {
      await showError();
    }
  }
}
