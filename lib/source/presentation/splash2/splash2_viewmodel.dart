import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../source.dart';

class Splash2ViewModel extends BaseViewModel {
  Map<DateTime, List<dynamic>> events = {};
  late RemoteConfig remoteConfig;
  late String home;

  init(bool isHome) async {
    try {
      setLoading(true);
      if (!isHome) {
        remoteConfig = await RemoteConfig.instance;
        var defaults = <String, dynamic>{
          'home': '',
        };

        await remoteConfig.setDefaults(defaults);
        remoteConfig.setConfigSettings(
          RemoteConfigSettings(
            minimumFetchIntervalMillis: 21600000,
            fetchTimeoutMillis: 30000,
          ),
        );

        await remoteConfig.fetch(expiration: Duration(hours: 0));
        await remoteConfig.activateFetched();
        home = remoteConfig.getString("home");
        if (home == "true") {
          print("UI Splash");
          await Future.delayed(Duration(milliseconds: 350));
          Navigator.pushReplacementNamed(context, Routers.splash);
        } else {
          _openHome();
        }
      } else {
        _openHome();
      }

      setLoading(false);
    } on FetchThrottledException {
      await showError(msg: "Lỗi kết nối mạng");
      setLoading(false);
    } catch (e) {
      await showError(msg: "Lỗi khác");
      setLoading(false);
    }
  }

  _openHome() async {
    print("UI Home");
    events = Map<DateTime, List<dynamic>>.from(await AppShared.decodeMapEvent());
    await Future.delayed(Duration(milliseconds: 350));

    Navigator.pushReplacementNamed(
      context,
      Routers.home,
      arguments: events,
    );
  }
}
