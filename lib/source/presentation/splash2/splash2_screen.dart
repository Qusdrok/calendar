import 'package:flutter/material.dart';
import 'package:flutter_calendar/source/source.dart';

class Splash2Screen extends StatefulWidget {
  final bool isHome;

  const Splash2Screen({this.isHome = false});

  @override
  _Splash2ScreenState createState() => _Splash2ScreenState();
}

class _Splash2ScreenState extends State<Splash2Screen> {
  late Splash2ViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget(
      viewModel: new Splash2ViewModel(),
      onViewModelReady: (viewModel) =>
          _viewModel = (viewModel as Splash2ViewModel)..init(widget.isHome),
      builder: (context, viewModel, child) {
        return WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            body: SafeArea(
              child: Center(
                child: _buildBody(),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AppImages.logo2,
            height: AppSize.screenHeight / 3,
            width: AppSize.screenWidth / 1.25,
          ),
          Text(
            "WELCOME TO MY APP",
            style: AppStyles.DEFAULT_XLARGE.copyWith(
              color: AppColors.primaryDark,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
