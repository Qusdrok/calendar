import 'package:flutter/material.dart';

import '../source.dart';

class WidgetTextFormField extends StatelessWidget {
  final String hintText;
  final TextStyle? hintStyle;
  final bool obscureText;
  final bool readOnly;
  final String? suffixImg;
  final String? prefixImg;
  final EdgeInsets? paddingImg;
  final FormFieldValidator<String>? validator;
  final TextEditingController controller;
  final double height;
  final double circular;
  final Color? colorBorder;
  final Function? onTap;
  final ValueChanged<String>? onSubmitted;

  WidgetTextFormField({
    required this.hintText,
    this.circular = 150,
    this.height = 16,
    this.colorBorder,
    required this.controller,
    this.suffixImg,
    this.prefixImg,
    this.readOnly = false,
    this.hintStyle,
    this.obscureText = false,
    this.onSubmitted,
    this.paddingImg,
    this.validator,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    var _borderTextField = _buildBorderTextField(
      color: colorBorder ?? AppColors.primaryVeryVeryLight,
      circular: circular,
    );

    return TextFormField(
      onTap: () => onTap,
      controller: controller,
      readOnly: readOnly,
      obscureText: obscureText,
      onFieldSubmitted: onSubmitted,
      validator: validator,
      decoration: InputDecoration(
        suffixIcon: suffixImg != null
            ? Padding(
                padding: paddingImg ?? EdgeInsets.all(13),
                child: Image.asset(
                  suffixImg ?? "",
                  width: 20,
                  height: 20,
                  fit: BoxFit.fill,
                ),
              )
            : null,
        prefixIcon: prefixImg != null
            ? Padding(
                padding: paddingImg ?? EdgeInsets.fromLTRB(25, 13, 13, 13),
                child: Image.asset(
                  prefixImg ?? "",
                  width: 20,
                  height: 20,
                  fit: BoxFit.fill,
                ),
              )
            : null,
        focusedBorder: _borderTextField,
        enabledBorder: _borderTextField,
        errorBorder: _borderTextField,
        focusedErrorBorder: _borderTextField,
        contentPadding: EdgeInsets.symmetric(
          horizontal: 25,
          vertical: height,
        ),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: hintStyle ??
            AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.primaryVeryVeryLight,
              fontSize: 16,
            ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField({
    required Color color,
    required double circular,
  }) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(circular),
      borderSide: BorderSide(
        color: color,
        width: 0.85,
      ),
    );
  }
}
