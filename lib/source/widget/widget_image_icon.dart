import 'package:flutter/material.dart';

import '../source.dart';

class WidgetImageIcon extends StatelessWidget implements Icon {
  final String imageURL;
  final double height;
  final double width;
  final BoxFit fit;

  WidgetImageIcon({
    key,
    required this.imageURL,
    required this.height,
    required this.width,
    this.fit = BoxFit.fill,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      imageURL,
      width: width,
      height: height,
      fit: fit,
      color: AppColors.primaryVeryLight,
    );
  }

  @override
  // TODO: implement color
  Color? get color => throw UnimplementedError();

  @override
  // TODO: implement icon
  IconData? get icon => throw UnimplementedError();

  @override
  // TODO: implement semanticLabel
  String? get semanticLabel => throw UnimplementedError();

  @override
  // TODO: implement size
  double? get size => throw UnimplementedError();

  @override
  // TODO: implement textDirection
  TextDirection? get textDirection => throw UnimplementedError();
}
