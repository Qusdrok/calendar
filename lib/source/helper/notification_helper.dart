/*
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/timezone.dart' as tz;

class NotificationHelper {
  static late FlutterLocalNotificationsPlugin _localNotification;
  static late AndroidInitializationSettings _androidSetting;
  static late IOSInitializationSettings _iosSetting;
  static late InitializationSettings _initSetting;
  static late AndroidNotificationDetails _androidNotificationDetails;
  static late IOSNotificationDetails _iosNotificationDetails;
  static late NotificationDetails _notificationDetails;
  static late int _idNotification = 0;

  static void initNotificationManager() {
    _androidSetting = new AndroidInitializationSettings('@mipmap/ic_launcher');
    _iosSetting = new IOSInitializationSettings();
    _initSetting = new InitializationSettings(
      android: _androidSetting,
      iOS: _iosSetting,
    );

    _androidNotificationDetails = AndroidNotificationDetails(
      'channel id',
      'channel name',
      'channel description',
      importance: Importance.max,
      priority: Priority.high,
    );

    _iosNotificationDetails = new IOSNotificationDetails();
    _notificationDetails = new NotificationDetails(
      android: _androidNotificationDetails,
      iOS: _iosNotificationDetails,
    );

    _localNotification = new FlutterLocalNotificationsPlugin();
    _localNotification.initialize(_initSetting);
    tz.initializeDatabase([]);
  }

  static Future<void> showNotification({
    required DateTime dt,
    required String title,
    required String body,
  }) async {
    await _localNotification.zonedSchedule(
      0,
      title,
      body,
      _instanceOfScheduleTime(),
      _notificationDetails,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  static Future<void> cancelNotification(int id) async {
    await _localNotification.cancel(id);
  }

  static tz.TZDateTime _instanceOfScheduleTime() {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate = tz.TZDateTime(tz.local, now.year, now.month, now.day, 10);

    if (scheduledDate.isBefore(now)) scheduledDate = scheduledDate.add(const Duration(days: 1));
    return scheduledDate;
  }
}
*/
