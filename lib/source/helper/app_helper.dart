import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../main.dart';

class AppHelper {
  AppHelper._();

  static final formatter = new NumberFormat("#,###");

  static String convertNumber(int amount) {
    return formatter.format(amount);
  }

  static void goToHome(BuildContext context) => Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => MyApp()),
        (route) => false,
      );

  static String convertDateTime2String(
    DateTime dateTime, {
    String format = 'dd/MM/yyyy',
  }) {
    if (dateTime == null) return "";
    return DateFormat(format).format(dateTime);
  }

  static DateTime? convertString2DateTime(String dateTime,
      {String format = "yyyy-MM-ddTHH:mm:ss.SSSZ"}) {
    if (dateTime == null) return null;
    return DateFormat(format).parse(dateTime);
  }

  static String convertString2String(
    String dateTime, {
    String inputFormat = "yyyy-MM-ddTHH:mm:ss.SSSZ",
    String outputFormat = "yyyy-MM-dd",
  }) {
    if (dateTime == null) return "";
    final input = convertString2DateTime(dateTime, format: inputFormat);
    return convertDateTime2String(input!, format: outputFormat);
  }

  static String minimum(int value) {
    if (value == null) return "00";
    return value < 10 ? "0$value" : "$value";
  }

  static String convertPhoneNumber(
    String phone, {
    String code = "+84",
    String hide = "***",
    bool isHide = true,
    bool isCenter = true,
    bool isCode = true,
  }) {
    if (isHide) {
      if (isCenter) {
        phone = phone.replaceRange(3, phone.length - 3, hide);
      } else {
        phone = phone.replaceRange(
            phone.length - (phone.length < 5 ? 0 : 5), phone.length, hide);
      }
    }

    return '$code${phone.substring(1)}';
  }

  static String convertGmail(String gmail, {String hide = "*****"}) {
    RegExp regex = new RegExp(r"(?=@).+(?=$)");
    String result;

    result = regex
        .allMatches(gmail)
        .map((e) => e.group(0))
        .toString()
        .replaceAll("(", "")
        .replaceAll(")", "");
    if (!regex.hasMatch(gmail)) gmail.replaceAll(regex, "");
    gmail = gmail.replaceRange(3, gmail.length, hide);
    return "$gmail$result";
  }

  static Map<String, dynamic> mapData(Map<String, dynamic> data) {
    return {"data": data};
  }

  static Map<String, dynamic> parseData(Map<String, dynamic> data) {
    return data['data'] ?? {};
  }
}
