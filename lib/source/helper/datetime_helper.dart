import 'package:intl/intl.dart';

class DateTimeHelper {
  static List<String> _monthsToEnglish = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  static String ConvertMonthToEnglish(DateTime dt) => _monthsToEnglish[dt.month - 1];

  static String ConvertDateTimeWithTextMonth(DateTime dt) => DateFormat.yMMMMd().format(dt);

  static String ConvertStringToMonth(int index) => _monthsToEnglish[index - 1];

  static String ConvertDateTime(
    DateTime dt, {
    String format = "dd/MM/yyyy",
  }) =>
      DateFormat(format).format(dt);
}
