import 'dart:async';
import 'dart:convert';

import 'package:flutter_calendar/source/helper/app_helper.dart';
import 'package:rx_shared_preferences/rx_shared_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../source.dart';

class AppShared {
  AppShared._();

  static final _prefs = RxSharedPreferences(SharedPreferences.getInstance());

  static const String _keyEvent = "keyEvent9";

  static Future<List<CalendarEvent>?> getEvent(DateTime dt) async {
    String json = await _prefs.getString(_keyEvent);
    print("Json saved: ${json}");

    if (json.length > 0) {
      return CalendarEvent.listFromJson(
          jsonDecode(json)['${AppHelper.convertDateTime2String(dt)}']);
    } else {
      return null;
    }
  }

  static Stream<String> getEventStream(DateTime dt) {
    return _prefs.getStringStream(_keyEvent).transform(
      StreamTransformer.fromHandlers(
        handleData: (data, sink) {
          var json = jsonDecode(data)['${dt.toIso8601String()}'];
          return json == null ? sink.add('') : sink.add(jsonEncode(json));
        },
      ),
    );
  }

  static Future<bool> setEvent(Map<DateTime, dynamic> events) async {
    var json = events != null ? jsonEncode(encodeMap(events)) : "";
    print("Save json ${json}");
    return await _prefs.setString(_keyEvent, json);
  }

  static Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toIso8601String()] = map[key];
    });

    return newMap;
  }

  static Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  static Future<Map<DateTime, dynamic>> decodeMapEvent() async {
    var key = await _prefs.getString(_keyEvent);
    if (key != null) return decodeMap(json.decode(key));
    return decodeMap(json.decode("{}"));
  }
}
