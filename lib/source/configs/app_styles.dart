import 'package:flutter/material.dart';

import '../source.dart';

class AppStyles {
  AppStyles._();

  static const String FONT_ROBOTO = "Roboto";

  static const FONT_SIZE_VERY_SMALL = 10.0;
  static const FONT_SIZE_SMALL = 12.0;
  static const FONT_SIZE_MEDIUM = 14.0;
  static const FONT_SIZE_REGULAR = 16.0;
  static const FONT_SIZE_LARGE = 18.0;
  static const FONT_SIZE_XLARGE = 20.0;

  static const TEXT_HEIGHT = 1.2;

  static const DEFAULT_VERY_SMALL = TextStyle(
    fontSize: FONT_SIZE_VERY_SMALL,
    height: TEXT_HEIGHT,
    fontFamily: FONT_ROBOTO,
  );
  static const DEFAULT_SMALL = TextStyle(
    fontSize: FONT_SIZE_SMALL,
    height: TEXT_HEIGHT,
    fontFamily: FONT_ROBOTO,
  );
  static const DEFAULT_MEDIUM = TextStyle(
    fontSize: FONT_SIZE_MEDIUM,
    height: TEXT_HEIGHT,
    fontFamily: FONT_ROBOTO,
  );
  static const DEFAULT_REGULAR = TextStyle(
    fontSize: FONT_SIZE_REGULAR,
    height: TEXT_HEIGHT,
    fontFamily: FONT_ROBOTO,
  );
  static const DEFAULT_LARGE = TextStyle(
    fontSize: FONT_SIZE_LARGE,
    height: TEXT_HEIGHT,
    fontFamily: FONT_ROBOTO,
  );
  static const DEFAULT_XLARGE = TextStyle(
    fontSize: FONT_SIZE_XLARGE,
    height: TEXT_HEIGHT,
    fontFamily: FONT_ROBOTO,
  );

  static final DEFAULT_VERY_SMALL_BOLD = DEFAULT_VERY_SMALL.copyWith(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
  static final DEFAULT_SMALL_BOLD = DEFAULT_SMALL.copyWith(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
  static final DEFAULT_MEDIUM_BOLD = DEFAULT_MEDIUM.copyWith(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
  static final DEFAULT_REGULAR_BOLD = DEFAULT_REGULAR.copyWith(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
  static final DEFAULT_LARGE_BOLD = DEFAULT_LARGE.copyWith(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
  static final DEFAULT_XLARGE_BOLD = DEFAULT_XLARGE.copyWith(
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );
}
