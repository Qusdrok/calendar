class AppImages {
  AppImages._();

  static final String logo = "assets/logo.jpg";
  static final String logo2 = "assets/logo_2.png";

  static final String icChevronLeft = "assets/chevron_left.png";
  static final String icChevronRight = "assets/chevron_right.png";

  static final String btnHome = "assets/home.png";
  static final String btnSupport = "assets/support.png";
}
