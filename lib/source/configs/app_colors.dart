import 'package:flutter/material.dart';

import '../source.dart';

class AppColors {
  AppColors._();

  static final Color primary = HexColor.fromHex("#403a60");
  static final Color primaryDark = HexColor.fromHex("#38324e");
  static final Color primaryLight = HexColor.fromHex("#875fc0");
  static final Color primaryVeryLight = HexColor.fromHex("#bcb8d1");
  static final Color primaryVeryVeryLight = HexColor.fromHex("#6e688e");

  static final Color orange = HexColor.fromHex("#ff9750");
  static final Color lightOrange = HexColor.fromHex("#ffb72a");

  static final Color pink = HexColor.fromHex("#eb4985");
  static final Color blue = HexColor.fromHex("#45c5f4");

  static final Color black = HexColor.fromHex("#212121");
  static final Color white = HexColor.fromHex("#faf9ff");
  static final Color red = HexColor.fromHex("#A43732");
}
